var gulp = require('gulp');
var gutil = require('gulp-util');
var source = require('vinyl-source-stream');
var browserify = require('browserify');
var watchify = require('watchify');
var reactify = require('reactify');
var notifier = require('node-notifier');
var server = require('gulp-server-livereload');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var watch = require('gulp-watch');

// gulp to concat
var concat = require('gulp-concat');

// console logging
var gulp_util =require('gulp-util');

// require modules
var browserify = require('browserify');

var source = require('vinyl-source-stream');

// re-run the gulp file
var watchify = require('watchify');

// work with browserify
var reactify = require('reactify');

// new tasks for gulp
// it is like writing functions
gulp.task('build', function()
{
    // object that holds config

    // bundler..watches the entries
    var bundler = watchify(browserify({
        entries: ['./src/app.jsx'],

        // transformation to the files..pipes...reactify
        transform: [reactify],
        extensions: ['.jsx'],
        debug: true,
        cache:{},
        packageCache:{},
        fullPaths: true
    }));

    var build = function(file){

        // get the entry from the pipe
        if(file){
            gulp_util.log('Recompiling ' + file)
        }

        return bundler
            // save the config
            .bundle()
            .on('error', gulp_util.log.bind(gulp_util, 'Error'))
            .pipe(source('main.js'))
            .pipe(gulp.dest('./'))
    }

    build()

    bundler.on('update', build);
});

gulp.task('serve', function(done) {
    gulp.src('')
        .pipe(server({
            livereload: {
                enable: true,
                filter: function(filePath, cb) {
                    if(/main.js/.test(filePath)) {
                        cb(true)
                    } else if(/style.css/.test(filePath)){
                        cb(true)
                    }
                }
            },
            open: true
        }));
});

gulp.task('default', ['build', 'serve']);