// store is responsible for data
var React = require('react');
var Api = require('../utils/api');
var Reflux = require('reflux');


// just like createClass
module.exports = Reflux.createStore({

    // make a store called topics
    // returns promise
    // store it in TopicStore.topics
    getTopics: function() {
        return Api.get('topics/defaults')
            .then(function(json){
                this.topics = json.data;
                this.triggerChange();
            }.bind(this));
    },
    triggerChange: function() {
        this.trigger('change', this.topics);
    }
})