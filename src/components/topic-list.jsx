var React = require('react');
var TopicStore = require('../store/topic-store');
var Reflux = require('reflux');


module.exports = React.createClass({

    mixins:[
        Reflux.listenTo(TopicStore, 'onchange')
    ],

    getInitialState: function(){
    return {topics:[]};
    },

    componentWillMount: function() {
        TopicStore.getTopics();
    },

    render: function() {
        return <div className="list-group">
            Topic List
            //{this.renderTopics()}
        </div>
    },

    renderTopics: function() {
        return this.state.topics.map(function(topic){
            return <li>
                {topic}
            </li>
        });
    },

    onChange : function(evt, topics){
        this.setState({
            topics: TopicStore.topics.map(function(topic){
                return topic.name})})
    }
});