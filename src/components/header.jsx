var React = require('react');
var ReactDOM = require('react-dom');
var Router = require('react-router');

// Avoid full refresh. Replace <a href> to <link to>
var Link = Router.Link;


// Module Export..is weird
// React Class -> component
module.exports = React.createClass({

    // react component lifecycle
    componentWillMount: function()
    {
    },

    // component initial state
    getInitialState: function() {
        return {

        }
    },

    // Header is from the Bootstrap nav bar
    // directly injecting the template into the render function
    render: function() {
        return <div><nav className="navbar navbar-default">
            <div className="container-fluid">
                <div className="navbar-header">
                    <Link className="navbar-brand" to="/">Imgur Browser</Link>
                    <ul className="nav navbar-nav nav-bar-right">
                        <li className="active">
                            <Link to="#">Link</Link></li>
                    </ul>
                </div>
            </div>
        </nav></div>
    }
});
