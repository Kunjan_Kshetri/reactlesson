var React = require('react');
var ReactDOM = require('react-dom');
var Routes = require("./routes")

// The app just renders the routes
ReactDOM.render(Routes, document.querySelector('.container'));
