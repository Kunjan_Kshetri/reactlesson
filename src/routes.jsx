var React = require('react');
var Main = require('./components/main')

// library
var ReactRouter = require('react-router')

// Router is the actual router object
var Router = ReactRouter.Router

// Configure the router
var Route = ReactRouter.Route


// Routes Object
module.exports = (
    <Router>
        <Route path="/" component={Main}>

        </Route>
    </Router>
);